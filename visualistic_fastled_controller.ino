#include <FastLED.h>

#define LED_PIN     8
#define NUM_LEDS    8
#define LED_TYPE    WS2812B
#define COLOR_ORDER GRB

#define BUTTON_PIN_1 2
#define BUTTON_PIN_2 3
#define POT_PIN_1 0
#define POT_PIN_2 1

uint8_t lastButtonState_1 = LOW;
uint8_t buttonState_1;
bool buttonPressed_1 = false;
unsigned long lastDebounceTime_1;

uint8_t lastButtonState_2 = LOW;
uint8_t buttonState_2;
bool buttonPressed_2 = false;
unsigned long lastDebounceTime_2;

uint8_t potVal_1 = 0;
uint8_t potVal_2 = 0;

// led stuff
CRGB leds[ NUM_LEDS ];
CRGBPalette16 currentPalette;
TBlendType currentBlending;

uint8_t mode = 0;
uint8_t palette = 1;

void setup() {

    delay( 2000 ); // power-up safety delay

    FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>( leds, NUM_LEDS ).setCorrection( TypicalLEDStrip );
    currentPalette = RainbowColors_p;
    currentBlending = LINEARBLEND;

    pinMode( BUTTON_PIN_1, INPUT );
    pinMode( BUTTON_PIN_2, INPUT );

    Serial.begin( 9600 );
}

void loop() {

    readInterrupt();

    switch ( mode ) {
        case 0:
            faslLEDPalette();
            break;
        case 1:
            hsvColor();
            break;
        }
    FastLED.show();
}

void readInterrupt () {

    if ( readButtonOne() ) {
        mode = 0;
        switchPalette();
    }
    if ( readButtonTwo() ) {
        mode = 1;
    }
}

void hsvColor () {

    int hue = map( smoothAnalogRead( POT_PIN_1 ), 0, 1023, 0, 255 );
    int brightness = map( smoothAnalogRead( POT_PIN_2 ), 0, 1023, 1, 255 );


    for( int i = 0; i <= NUM_LEDS; i++ ) {
        leds[i] = CHSV( hue, 255, brightness );
    }
}

void faslLEDPalette () {

    static uint8_t startIndex = 0;
    int speed = map( smoothAnalogRead( POT_PIN_1 ), 0, 1023, 1 , 15 );
    startIndex = startIndex + speed; /* motion speed */

    FillLEDsFromPaletteColors( startIndex );
}

void FillLEDsFromPaletteColors( uint8_t colorIndex ) {

    int brightness = map( smoothAnalogRead( POT_PIN_2 ), 0, 1023, 1, 255 );

    for( int i = 0; i < NUM_LEDS; i++) {
        leds[i] = ColorFromPalette( currentPalette, colorIndex, brightness, currentBlending );
        colorIndex -= 3;
        readInterrupt();
    }
}

void switchPalette () {

    palette++;

    if ( palette > 8 ) {
        palette = 1;
    }

    switch ( palette ) {
        case 1:
            currentPalette = PartyColors_p;
            break;
        case 2:
            currentPalette = LavaColors_p;
            break;
        case 3:
            currentPalette = CloudColors_p;
            break;
        case 4:
            currentPalette = HeatColors_p;
            break;
        case 5:
            currentPalette = ForestColors_p;
            break;
        case 6:
            currentPalette = OceanColors_p;
            break;
        case 7:
            currentPalette = RainbowColors_p;
            break;
        case 8:
            currentPalette = RainbowStripeColors_p;
            break;
    }
 }

int smoothAnalogRead ( int analogPin ) {

    uint8_t numReadings = 10;
    int reading = 0;

    // smoothing the pot reading
    for (size_t i = 0; i < numReadings; i++) {
        reading += analogRead( analogPin );
        delay( 1 );
    }
    reading = reading / numReadings;
    return reading;
}

bool readButtonOne () {

    int reading = digitalRead( BUTTON_PIN_1 );

    if ( reading != lastButtonState_1 ) {
        lastDebounceTime_1 = millis();
    }
    if ( ( millis() - lastDebounceTime_1 ) > 20 ) { // debounce delay 20

        if ( reading != buttonState_1 ) {
            buttonState_1 = reading;

            if ( buttonState_1 == HIGH ) {
                buttonPressed_1 = true;
                return false;
            }

            if ( buttonState_1 == LOW && buttonPressed_1 ) {

                buttonPressed_1 = false;
                return true;
            }
        }
    }
    lastButtonState_1 = reading;
}

bool readButtonTwo () {

    int reading = digitalRead( BUTTON_PIN_2 );

    if ( reading != lastButtonState_2 ) {
        lastDebounceTime_2 = millis();
    }
    if ( ( millis() - lastDebounceTime_2 ) > 20 ) { // debounce delay 20


        if ( reading != buttonState_2 ) {
            buttonState_2 = reading;

            if ( buttonState_2 == HIGH ) {
                buttonPressed_2 = true;
                return false;
            }

            if ( buttonState_2 == LOW && buttonPressed_2 ) {

                buttonPressed_2 = false;
                return true;
            }
        }
    }
    lastButtonState_2 = reading;
}
